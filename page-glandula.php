<?php // Template Name: Glandula ?> 

<?php 
    get_header();
?>
<?php get_template_part( 'templates/menu-interno' ); ?>
<main class="main-glandula">
    <div class="glandula">
        <section class="banner-glandula">
            
        </section>
        <section class="chamada-lab container">
            <h2>Sobre a Glândula.Lab</h2>
            <p><?php the_field('conteudo-glandula-destaque')?></p>
        </section>
        <section class="galeria-glandula">
            <div class="cont-galeria-glandula container">
                <div class="col-img-destaque">
                    <a class="link-img" href="<?php the_field('imagem-glandula-destaque')?>" data-lightbox="example-set" data-title="">
                        <img class="example-image img-destacada" src="<?php the_field('imagem-glandula-destaque')?>" alt=""/>
                    </a>
                </div>
                <div class="col-thumb">
                    <ul class="list-unstyled">
                        <?php if(have_rows('galeria-glandula')): while(have_rows('galeria-glandula')) : the_row(); ?>
                            <li>
                                <a class="link-img" href="<?php the_sub_field('imagem-glandula')?>" data-lightbox="example-set" data-title="">
                                    <img class="example-image img-destacada" src="<?php the_sub_field('imagem-glandula')?>" alt=""/>
                                </a>
                            </li>
                        <?php endwhile; else : endif; ?>
                    </ul>
                </div>
            </div>
        </section>
        <section class="video-lab container">
            <h2>Vídeo.Labs</h2>
            <div class="cont-video-lab">
                <?php if(have_rows('videos-glandula')): while(have_rows('videos-glandula')) : the_row(); ?>
                    <div class="item-video">
                        <?php the_sub_field('video-glandula')?>
                    </div>
                <?php endwhile; else : endif; ?>
            </div>
        </section>
        <section class="agendar-lab container">
            <h2>Agendar residência de gravação</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga vero eu lorem Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat incidunt ducimus, natus dicta accusantium obcaecati sunt corrupti sint repudiandae temporibus nesciunt quaerat, et nisi consequuntur. Inventore molestias eos cum dignissimos. m explicabo reiciendis? Quia esse voluptate officia quidem iure repellat vero quibusdam in exercitationem animi facere, ullam corporis debitis. Labore?</p>
            <div class="btn-agendar">
                <a href="">Agende sua residência</a>
            </div>
        </section>
    </div>   
</main>

<?php get_footer()?>
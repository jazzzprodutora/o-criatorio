<?php // Template Name: Residência Artistica Agende ?> 

<?php 
    get_header();
?>
<?php get_template_part( 'templates/menu-interno' ); ?>
<main class="residencia">
    <section class="banner-residencia">
        <h2>Residências Artísicas</h2>
    </section>
    <section class="agendar container">
        <h2>Solicite sua <br>Residência Artística</h2>
        <?php echo do_shortcode( '[contact-form-7 id="77" title="Agendar Residência Artística"]' ); ?>

    </section>
</main>

<?php get_footer()?>
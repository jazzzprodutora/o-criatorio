<?php // Template Name: Residência Artistica ?> 

<?php 
    get_header();
?>
<?php get_template_part( 'templates/menu-interno' ); ?>
<main class="main-residencia">
    <div class="residencia">
        <section class="banner-residencia">
            <h2>Residências Artísicas</h2>
        </section>
        <section class="chamada-residencia container">
            <div class="cont-residencia">
                <h2>Residências <br> Artísticas</h2>
                <p><?php the_field('chamada-residencia')?></p>
            </div>
        </section>
        <section class="espacos container">
            <div class="title-flex">
                <h2>Nossos Espaços</h2>
            </div>
            <?php if(have_rows('espacos')): while(have_rows('espacos')) : the_row(); ?>
                <article class="item-espaco">
                    <div class="img-espaco">
                        <img src="<?php the_sub_field('img-espacos')?>" alt="">
                    </div>
                    <div class="info-espaco">
                        <h3><?php the_sub_field('titulo-espacos')?></h3>
                        <p><?php the_sub_field('conteudo-espacos')?></p>
                    </div>
                </article>
            <?php endwhile; else : endif; ?>
        </section>
        <section class="galeria container">
            <div class="title-flex">
                <h2>Galeria de Fotos</h2>
            </div>
                <div class="cont-galeria">
                    <?php if(have_rows('galeria-residencia')): while(have_rows('galeria-residencia')) : the_row(); ?>
                        <a class="link-img" href="<?php the_sub_field('imagem-residencia')?>" data-lightbox="example-set" data-title="">
                            <img class="example-image img-destacada" src="<?php the_sub_field('imagem-residencia')?>" alt=""/>
                        </a>
                    <?php endwhile; else : endif; ?>
                </div>
        </section>
        <section class="agende">
            <a href="<?php bloginfo('url') ?>/residencias-artisticas/agende/">Agende sua residência</a>
        </section>
    </div>
</main>

<?php get_footer()?>

<?php 
    get_header();
?>
<?php get_template_part( 'templates/menu-interno' ); ?>
<main class="curso-destaque">
    <section class="banner-retiros">
        <h2>Atividades</h2>
    </section>
    <section class="conteudo-destaque container">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <article class="curso-destaque-item">
            <h2><?php the_title()?></h2>
            <div class="img-destaque">
                <?php the_post_thumbnail()?>
            </div>
            <?php the_content()?>
            <?php echo do_shortcode("[sharify]"); ?>

        </article>
        <?php endwhile;?> <?php endif; ?>
        <aside class="ultimos-cursos">
            <ul class="list-unstyled">
                <li>Outras Atividades</li>
                <?php
                    $args = array (
                        'post_type' => 'atividade',
                    );
                    $the_query = new WP_Query ( $args );?>
                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <li><a href="<?php the_permalink();?>"> <?php the_title()?> </a></li>
                <?php endwhile;?> <?php endif; ?>
                <?php  wp_reset_postdata();?>
                
            </ul>
        </aside>
    </section>
</main>

<?php get_footer()?>
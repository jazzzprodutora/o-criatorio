<?php // Template Name:Cursos ?> 

<?php 
    get_header();
?>
<?php get_template_part( 'templates/menu-interno' ); ?>
<main class="main-curso">
    <div class="curso">
        <section class="banner-curso">
            <h2>Cursos</h2>
        </section>
        <section class="cursos-page container">
            <div class="title-flex">
                <h2>Cursos</h2>
            </div>
            <div class="content-cursos-page">
                <?php
                    $args = array (
                        'post_type' => 'curso',
                    );
                    $the_query = new WP_Query ( $args );?>
                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <article>
                        <a href="<?php the_permalink();?>">
                            <?php the_post_thumbnail()?>
                            <div class="titulo-curso-home">
                                <h4><?php the_title()?></h4>
                                <p><?php the_excerpt();?></p>
                                <span class="saiba">Saiba + </span>
                            </div>
                        </a>
                    </article>
                <?php endwhile;?> <?php endif; ?>
                <?php  wp_reset_postdata();?>
            </div>
        </section>
    </div>
</main>

<?php get_footer()?>
<?php // Template Name: Sobre ?> 

<?php 
    get_header();
?>
<?php get_template_part( 'templates/menu-interno' ); ?>
<main class="main-sobre">
    <div class="sobre">
        <section class="banner-sobre">
            <h2>Conheça o criatório</h2>
        </section>
        <section class="chamada-sobre container">
            
            <h2><?php the_field('chamada-titulo')?></h2>
            <p><?php the_field('chamada-conteudo')?></p>
        
        </section>
        <section class="principais-atividades container">
            <div class="title-flex">
                <div class="opacity">opacity</div>
                <h2>Principais atividades</h2>
                <a href="<?php bloginfo('url') ?>/atividade/">Ver Todas <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/arrow.png" alt=""></a>
            </div>
            <div class="cont-atividades">
                <?php
                    $args = array (
                        'post_type' => 'atividade',
                        'posts_per_page' => '6',
                    );
                    $the_query = new WP_Query ( $args );?>
                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <article>
                        <a href="<?php the_permalink();?>">
                            <?php the_post_thumbnail()?>
                            <div class="legenda-curso">
                                <h4><?php the_title()?></h4>
                                <p><?php the_excerpt();?></p>
                                <span class="saiba">Saiba + </span>
                            </div>
                        </a>
                    </article>
                <?php endwhile;?> <?php endif; ?>
                <?php  wp_reset_postdata();?>
            </div>  
        </section>
        <section class="sobre-video container">
            <?php the_field('video')?>
        </section>
        <section class="integrantes">
            <?php if(have_rows('integrantes')): while(have_rows('integrantes')) : the_row(); ?>
                <div class="cont-integrantes container">
                    <div class="img-integrantes">
                        <img src="<?php the_sub_field('img-integrante')?>" alt="<?php the_sub_field('nome-integrante')?>">
                    </div>
                    <div class="info-integrantes">
                    <div class="content">
                        <h3><?php the_sub_field('nome-integrante')?></h3>
                        <?php the_sub_field('sobre-integrante')?>
                    </div>
                    </div>
                </div>
            <?php endwhile; else : endif; ?>
        </section>
    </div>
</main>

<?php get_footer()?>
<footer>
    <div class="cont-footer container">
        <div class="mapa-site">
            <ul class="list-unstyled">
                <li><h4>Mapa do site</h4></li>
                <li><a href="<?php bloginfo('url') ?>/sobre/">Conheça o Criatório</a></li>
                <li><a href="<?php bloginfo('url') ?>/retiros-e-oficinas/">retiros e Oficinas</a></li>
                <li><a href="<?php bloginfo('url') ?>/residencias-artisticas/">Residências Artísticas</a></li>
                <li><a href="<?php bloginfo('url') ?>/glandula/">Home Estúdio</a></li>
                <li><a href="<?php bloginfo('url') ?>/fale-conosco/">Fale Conosco</a></li>
            </ul>
        </div>
            <picture class="img-footer">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/criatorio-footer.jpg" alt="">
            </picture>
            <div class="news">
                <h4>Receba nossas novidades</h4>
                <div class="form-news">
                    <input type="email" name="email" placeholder="Informe seu Email">
                    <button type="submit">Ok</button>
                </div>
                <ul class="social-footer">
                    <h4>Siga nossas redes</h4>
                    <ul class="list-unstyled">
                        <li class="item-social"><a href="https://www.instagram.com/ocriatorio/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/instagram.png" alt="Instagram"></a></li>
                        <li class="item-social"><a href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/youtube.png" alt="Youtube"></a></li>
                        <li class="item-social"><a href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/facebook.png" alt="Facebook"></a></li>
                        <li class="item-social"><a href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/pinterest.png" alt="Pinterest"></a></li>
                    </ul>
            </div>
    </div>
    <div class="info-bottom-footer container">
        <ul class="list-unstyled">
            <li><a href="">Login |</a></li>
            <li><a href="">Políticas de privacidade</a></li>
        </ul>
    </div>
</footer>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/menu-lateral.js"></script>
    <?php wp_footer();?>
    </body>
</html>
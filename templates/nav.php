    <!-- Cabeçalho -->
    <header>
            <div class="header-top">
                <ul class="list-unstyled">
                    <li><a href="">Login</a></li>
                    <li><a href="">Cadastre-se</a></li>
                </ul>
            </div>
            <div class="cont-header container">
                <div class="logo">
                    <h1><a href="<?php bloginfo('url') ?>/home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/logo-criatorio.jpg" alt=""></a></h1>
                </div>
                <div class="icon-menu">
                    <input type="checkbox" id="menu-hamburger">
                    <label for="menu-hamburger">
                        <div class="menu">
                            <span class="hamburger"></span>
                        </div>
                    </label>
                </div>
            </div>
    </header>
<nav class="menu-nav">
    <ul class="list-unstyled">
        <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/conheca-icon.png" alt="">
            <a href="<?php bloginfo('url') ?>/sobre/">Conheça o Criatório</a>
        </li>
        <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/retiros-icon.png" alt="">    
            <a href="<?php bloginfo('url') ?>/retiros-e-oficinas/">retiros e Oficinas</a>
        </li>
        <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/residencia-icon.png" alt="">    
            <a href="<?php bloginfo('url') ?>/residencias-artisticas/">Residências Artísticas</a>
        </li>
        <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/home-studio.png" alt="">
            <a href="<?php bloginfo('url') ?>/glandula/">Home Estúdio</a>
        </li>
        <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/contato-icon.png" alt="">    
            <a href="<?php bloginfo('url') ?>/fale-conosco/">Fale Conosco</a>
        </li>
    </ul>
</nav>

<script>
    const btn = document.querySelector('.icon-menu');
    const nav = document.querySelector('.menu-nav');
    function menuHomeMobile(){
        nav.classList.add('menu-ativo');
    }
    btn.addEventListener('click', menuHomeMobile);
</script>
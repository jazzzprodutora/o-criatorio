    <!-- Cabeçalho -->
    <header>
            <div class="header-top">
                <ul class="list-unstyled">
                    <li><a href="">Login</a></li>
                    <li><a href="">Cadastre-se</a></li>
                </ul>
            </div>
            <div class="cont-header container">
                <div class="logo">
                    <h1><a href="<?php bloginfo('url') ?>/home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/logo-criatorio.jpg" alt=""></a></h1>
                </div>
                <div class="icon-menu-interno">
                    <input type="checkbox" id="menu-hamburger">
                    <label for="menu-hamburger">
                        <div class="menu">
                            <span class="hamburger"></span>
                        </div>
                    </label>
                </div>
            </div>
    </header>
    <nav class="menu-nav-interno">
    <ul class="list-unstyled container">
        <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/conheca-icon.png" alt="">
            <a href="<?php bloginfo('url') ?>/sobre/">Conheça o Criatório</a>
        </li>
        <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/retiros-icon.png" alt="">    
            <a href="<?php bloginfo('url') ?>/retiros-e-oficinas/">retiros e Oficinas</a>
        </li>
        <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/residencia-icon.png" alt="">    
            <a href="<?php bloginfo('url') ?>/residencias-artisticas/">Residências Artísticas</a>
        </li>
        <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/home-studio.png" alt="">
            <a href="<?php bloginfo('url') ?>/glandula/">Home Estúdio</a>
        </li>
        <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/contato-icon.png" alt="">    
            <a href="<?php bloginfo('url') ?>/fale-conosco/">Fale Conosco</a>
        </li>
        <li class="item-x"><span>X</span></li>
    </ul>
  
</nav>

<script>
    const btn = document.querySelector('.icon-menu-interno');
    const nav = document.querySelector('.menu-nav-interno');
    function menuHomeMobile(){
        nav.classList.add('menu-ativo-interno');
    }
    btn.addEventListener('click', menuHomeMobile);

    const btnFechar = document.querySelector('.item-x');
    function menuMobileFechar(){
        nav.classList.remove('menu-ativo-interno');
    }
    btnFechar.addEventListener('click', menuMobileFechar);
</script>
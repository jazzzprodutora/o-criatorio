<?php // Template Name: Contato ?> 

<?php 
    get_header();
?>
<?php get_template_part( 'templates/menu-interno' ); ?>
<main class="main-contato">
    <div class="page-contato">
        <section class="banner-contato">
            <h2>Fale Conosco</h2>
        </section>
        <section class="contato container">
            <div class="title-flex">
                <h2>Fale com <br> O criatório</h2>
            </div>
            <div class="cont-contato">
                <div class="col-form">
                    <?php echo do_shortcode( '[contact-form-7 id="76" title="Contato"]' ); ?>
                </div>
                <div class="col-address">
                    <h2>Criatório Atelier/Estúdio</h2>
                    <address>
                        <a href="" target="_blank"> 
                            <ul class="list-unstyled">
                                <li>Rua Agrestina, Nº 3000</li>
                                <li>Distrito de Cachoeirinha</li>
                                <li>Gravatá / PE</li>
                                <li>Cep 50340-400</li>
                            </ul>
                        </a>
                    </address>
                    <ul class="tel list-unstyled">
                        <li>Tel: <a href="tel:8130000000">(81) 3000-0000</a></li>
                        <li><a href="tel:81999009900">(81) 9 9900-9900</a></li>
                    </ul>
                    <div class="email">
                        <a href="mailto:contato@ocriatorio.com.br">contato@ocriatorio.com.br</a>
                    </div>
                    <ul class="social-contanto list-unstyled">
                        <li class="item-social"><a href="https://www.instagram.com/ocriatorio/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/instagram.png" alt="Instagram"></a></li>
                        <li class="item-social"><a href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/youtube.png" alt="Youtube"></a></li>
                        <li class="item-social"><a href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/facebook.png" alt="Facebook"></a></li>
                        <li class="item-social"><a href=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/pinterest.png" alt="Pinterest"></a></li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="mapa">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63182.86052265139!2d-35.599469472633615!3d-8.209917134124918!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7aa4820180cfe83%3A0xbb9e178bc01d927!2zR3JhdmF0w6EsIFBF!5e0!3m2!1spt-BR!2sbr!4v1611596949752!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </section>
    </div>
</main>

<?php get_footer()?>
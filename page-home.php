<?php // Template Name: Home ?> 

<?php 
    get_header();
?>
<main class="home-main">
    <?php get_template_part( 'templates/nav' ); ?>
    <div class="home-page">
        <section class="banner">
            <div class="cont-banner container">
                <div class="info-banner">
                    <h2>Espaço Criativo</h2>
                    <p>O Critaório é um espaço misto destinado à realização de cursos, residência artísticas, oficinas, gravações sonoras (@glandula.lab) e audiovisuais - Gravatá/PE</p>
                    <a href="">Saiba +</a>
                </div>
                <div class="img-banner">
                    <div id="carouselExampleControls" class="carousel slide carousel-fade " data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/banner-01.jpg" alt="">
                            </div>
                            <div class="carousel-item">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/criatorio-footer.jpg" alt="">
                            </div>
                            <div class="carousel-item">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/imgs/banner-01.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="chamada-home">
            <div class="cont-chamda-home container">
                <h2><?php the_field('titulo-chamada-home')?></h2>
                <p><?php the_field('chamada-home')?> </p>
                <?php the_field('video-chamada')?>
            </div>
        </section>
        <section class="curso-destaque-home container">
            <div class="title-flex">
                <h2>Cursos em destaque</h2>
                <a href="<?php bloginfo('url') ?>/curso/">Ver Todas <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/arrow.png" alt=""></a>
            </div>
            <div class="cont-curso-destaque">
                <?php
                    $args = array (
                        'post_type' => 'curso',
                        'posts_per_page' => '3',
                        'categoria_do_curso' => 'destaque',
                    );
                    $the_query = new WP_Query ( $args );?>
                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <article>
                        <a href="<?php the_permalink();?>">
                            <?php the_post_thumbnail()?>
                            <div class="legenda-curso">
                                <h4><?php the_title()?></h4>
                                <p><?php the_excerpt();?></p>
                                <span class="saiba">Saiba + </span>
                            </div>
                        </a>
                    </article>
                <?php endwhile;?> <?php endif; ?>
                <?php  wp_reset_postdata();?>
            </div>  
        </section>
        <section class="noticia-home container">
            <div class="title-flex">
                <h2>Notícias</h2>
                <a href="<?php bloginfo('url') ?>/noticia/">Ver Todas <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/arrow.png" alt=""></a>
            </div>
            <div class="content-noticia">
                <?php
                    $args = array (
                        'post_type' => 'noticia',
                        'posts_per_page' => '4',
                    );
                    $the_query = new WP_Query ( $args );?>
                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <article>
                        <a href="<?php the_permalink();?>">
                            <?php the_post_thumbnail()?>
                            <div class="titulo-noticia-home">
                                <h4><?php the_title()?></h4>
                                <p><?php the_excerpt();?></p>
                                <span class="saiba">Saiba + </span>
                            </div>
                        </a>
                    </article>
                <?php endwhile;?> <?php endif; ?>
                <?php  wp_reset_postdata();?>
            </div>
    </section>
    <section class="paralax">
    </section>

</div>
                </div>
                </main>
<?php get_footer()?>
<?php // Template Name: Retiros e Oficinas ?> 

<?php 
    get_header();
?>
<?php get_template_part( 'templates/menu-interno' ); ?>
<main class="main-retiros">
    <div class="retiros">
        <section class="banner-retiros">
            <h2>Retiros e Oficinas</h2>
        </section>
        <section class="chamada-retiros container">
            <div class="cont-retiros">
                <h2>Retiros <br> e Oficinas</h2>
                <p><?php the_field('conteudo-chamada')?></p>
            </div>
        </section>
        <section class="agenda-retiros container">
            <div class="title-flex">
                <div class="opacity">opacity</div>
                <h2>Agenda de cursos </h2>
                <a href="<?php bloginfo('url') ?>/curso/">Ver Todas <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/arrow.png" alt=""></a>
            </div>
            <div class="cont-agenda-retiros">
                <?php
                    $args = array (
                        'post_type' => 'curso',
                    );
                    $the_query = new WP_Query ( $args );?>
                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <article>
                        <a href="<?php the_permalink();?>">
                            <div class="img-data">
                                <?php the_post_thumbnail()?>
                                <span class="data-inicio">Início <?php the_field('data')?></span>
                            </div>
                            <div class="legenda-curso">
                                <h4><?php the_title()?></h4>
                                <p><?php the_excerpt();?></p>
                                <span class="saiba">Saiba + </span>
                            </div>
                        </a>
                    </article>
                <?php endwhile;?> <?php endif; ?>
                <?php  wp_reset_postdata();?>
            </div>  
        </section>
        <section class="curso-anteriores container">
            <div class="title-flex">
                <h2>Cursos Anteriores</h2>
            </div>
            <div class="cont-cursos-anteriores">
                <?php if(have_rows('add-curso')): while(have_rows('add-curso')) : the_row(); ?>
                    <article class="item-curso-anteriores">
                        <div class="video-curso-anteriores">
                            <?php the_sub_field('video-curso')?>
                        </div>
                        <div class="info-cursos-anteriores">
                            <h2><?php the_sub_field('titulo-curso')?></h2>
                            <p><?php the_sub_field('conteudo-curso')?></p>
                            <a class="saiba" href="">Tenho interesse no curso</a>
                        </div>
                    </article>
                <?php endwhile; else : endif; ?>
            </div>
        </section>
    </div>
</main>
<?php get_footer()?>
<?php // Template Name:Notícia ?> 

<?php 
    get_header();
?>
<?php get_template_part( 'templates/menu-interno' ); ?>

<main class="main-noticia">
    <div class="noticia">
        <section class="banner-noticia">
            <h2>notícias</h2>
        </section>
        <section class="noticias-page container">
            <div class="title-flex">
                <h2>Notícias</h2>
            </div>
            <div class="content-noticias-page">
                <?php
                    $args = array (
                        'post_type' => 'noticia',
                    );
                    $the_query = new WP_Query ( $args );?>
                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <article>
                        <a href="<?php the_permalink();?>">
                            <?php the_post_thumbnail()?>
                            <div class="titulo-noticia-home">
                                <h4><?php the_title()?></h4>
                                <p><?php the_excerpt();?></p>
                                <span class="saiba">Saiba + </span>
                            </div>
                        </a>
                    </article>
                <?php endwhile;?> <?php endif; ?>
                <?php  wp_reset_postdata();?>
            </div>
        </section>
    </div>
</main>

<?php get_footer()?>